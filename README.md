# Lab5 -- Integration testing


## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

Also you have to provide some kind of estimation of coverage you have done

Note: You can use any automated solution, but in this case I can ask whether you are sure that this solution does not contain the same bugs

Note 2: 10% of lab grade would be soundness of bugs you have found(in realtion to total amount of bugs injected in your version)

## Solution

My link for defaults: https://script.google.com/macros/s/AKfycbzJ6aOBK0uyU0Ry-KF3z4HMPQ6Ogqhz5QZLzxhvQmtiHE6BSZeWslfKN7mWsRmcom_s/exec?service=getSpec&email=d.kabdullina@innopolis.university

Spec: 
```
Budet car price per minute = 15
Luxury car price per minute = 34
Fixed price per km = 14
Allowed deviations in % = 13
Inno discount in % = 20
```

Results:
https://docs.google.com/spreadsheets/d/1-3pDdUpxIYOUHH8xW64GiriR4O82mZJ2lQNaETJQj6M/edit?usp=sharing
or in *SQR_Lab5_Dana.xlsx*

Errors:
1. Discount is not being counted in calculations
2. Allowed deviation is bigger than 13%, which was in my spec
3. Fixed price per km is not correct, it should be 14, however, according to responses, it is 12.5

